package com.zenoyuki.echorest.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class EchoTestController {
	@RequestMapping("/")
	public @ResponseBody String getFeedback() {
		return "EchoRest: ready";
	}
}
