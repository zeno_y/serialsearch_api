package com.zenoyuki.echorest.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.zenoyuki.echorest.entity.SectionType;
import com.zenoyuki.echorest.entity.SerialNum;
import com.zenoyuki.echorest.repository.RepoSectionType;
import com.zenoyuki.echorest.repository.RepoSerialNum;

@RestController
public class EchoController {
	
	@Autowired
	private RepoSerialNum serialNumRepo;
	
	@Autowired
	private RepoSectionType sectionTypeRepo;

	@GetMapping("/serial")
	public SerialNum getSerial(@RequestParam(value = "serial") String serialNum) {
		if(serialNum == null || serialNum.isEmpty()) return null;
		return serialNumRepo.findBySerialNum(serialNum);
	}
	
	@GetMapping("/type")
	public SectionType getType(@RequestParam(value = "id") long typeId) {
		return sectionTypeRepo.findById(typeId);
	}
	
	@GetMapping("/all-serial")
	public Iterable<SerialNum> getSerial() {
		return serialNumRepo.findAll();
	}
	
	@GetMapping("/all-types")
	public Iterable<SectionType> getTypes() {
		return sectionTypeRepo.findAll();
	}
}
