package com.zenoyuki.echorest.repository;

import org.springframework.data.repository.CrudRepository;

import com.zenoyuki.echorest.entity.Section;

public interface RepoSection extends CrudRepository<Section, Long> {

}
