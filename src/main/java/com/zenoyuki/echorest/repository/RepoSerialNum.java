package com.zenoyuki.echorest.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.zenoyuki.echorest.entity.SerialNum;

@Repository
public interface RepoSerialNum extends CrudRepository<SerialNum, Long> {
	@Query("SELECT s FROM SerialNum s WHERE s.serialNum = :serialNum")
	public SerialNum findBySerialNum(@Param("serialNum") String serialNum);
}
