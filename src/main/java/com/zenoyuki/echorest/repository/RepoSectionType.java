package com.zenoyuki.echorest.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.zenoyuki.echorest.entity.SectionType;

@Repository
public interface RepoSectionType extends CrudRepository<SectionType, Long> {
//	@Query("SELECT s FROM SectionType s WHERE s.id = :typeId")
	public SectionType findById(/*@Param("typeId")*/ long typeId);
}
