package com.zenoyuki.echorest.repository;

import org.springframework.data.repository.CrudRepository;

import com.zenoyuki.echorest.entity.Value;

public interface RepoValue extends CrudRepository<Value, Long> {

}
