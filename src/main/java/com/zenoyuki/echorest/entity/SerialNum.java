package com.zenoyuki.echorest.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
//@NamedQuery(name = "SerialNum.findBySerialNum", query = "SELECT s FROM SerialNum s WHERE s.serialNum = :serialNum")
@Table(name = "serial_num_table")
public class SerialNum {
	@JsonIgnore
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "_id")
	private long id;
	
	@Column(name = "serial_num")
	private String serialNum;
	
	@Column(name = "model")
	private String modelName;

	@OneToMany(mappedBy = "serialNum")
	private List<Section> sections;
	
	public SerialNum() {}
	
	public long getId() {
		return id;
	}
	
	public void setId(long id) {
		this.id = id;
	}
	
	public String getSerialNum() {
		return serialNum;
	}
	
	public void setSerialNum(String serialNum) {
		this.serialNum = serialNum;
	}
	
	public String getModelName() {
		return modelName;
	}
	
	public void setModelName(String modelName) {
		this.modelName = modelName;
	}

	public List<Section> getSections() {
		return sections;
	}

	public void setSections(List<Section> sections) {
		this.sections = sections;
	}
	
	public void addSection(Section section) {
		if(getSections() == null) setSections(new ArrayList<>());
		getSections().add(section);
//		section.setSerialNum(SerialNum.this);
	}
}
