package com.zenoyuki.echorest.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@Entity
@Table(name = "section_types_table")
@JsonPropertyOrder({"id", "name", "order", "hasButtons"})
public class SectionType {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "_id")
	private long id;
	
	@Column(name = "type")
	@JsonProperty("name")
	private String type;
	
	@Column(name = "has_btns_boolean")
	private boolean hasButtons;

	@OrderBy
	@Column(name = "order_val")
	private int order;
	
//	@JsonIgnore
//	@OneToMany(mappedBy = "sectionType")
//	private List<Section> sections;
	
	public SectionType() {}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public boolean getHasButtons() {
		return hasButtons;
	}

	public void setHasButtons(boolean hasButtons) {
		this.hasButtons = hasButtons;
	}

	public int getOrder() {
		return order;
	}

	public void setOrder(int order) {
		this.order = order;
	}
	
//	public void add(Section section) {
//		if(sections == null) sections = new ArrayList<>();
//		sections.add(section);
//		section.setSectionType(SectionType.this);
//	}
}
