package com.zenoyuki.echorest.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@Entity
@Table(name = "sections_table")
@JsonPropertyOrder({"typeId", "values"})
public class Section {
	@JsonIgnore
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "_id")
	private long id;
	
//	@Column(name = "type_id")
//	private long typeId;

	//Use this to pull section type ID to this entity
	@JsonIgnore
	@ManyToOne
	@JoinColumn(name = "type_id")
	private SectionType sectionType;
	
	@JsonProperty("typeId")
	private long getTypeId() {
		return sectionType.getId();
	}
	
	//Using above method, this pulls section type object data into this entity
//	@JsonProperty("type")
//	private String getType() {
//		return sectionType.getType();
//	}
//	
//	@JsonProperty("order")
	@OrderBy
	private int getOrder() {
		return sectionType.getOrder();
	}
//	
//	@JsonProperty("hasButtons")
//	private boolean getHasButtons() {
//		return sectionType.getHasButtons();
//	}
	
	@JsonIgnore //This prevents serial num data from showing in JSON. While this is fine, I'm not sure if this is the accepted solution
	@ManyToOne
	@JoinColumn(name = "serial_num_id")
	private SerialNum serialNum;
	
	@OneToMany(mappedBy = "section")
	private List<Value> values;
	
	public Section() {}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public SerialNum getSerialNum() {
		return serialNum;
	}

	public void setSerialNum(SerialNum serialNum) {
		this.serialNum = serialNum;
	}

//	public long getTypeId() {
//		return typeId;
//	}
//
//	public void setTypeId(long typeId) {
//		this.typeId = typeId;
//	}

	public List<Value> getValues() {
		return values;
	}

	public void setValues(List<Value> values) {
		this.values = values;
	}
	
	public void addValue(Value value) {
		if(getValues() == null) setValues(new ArrayList<>());
		getValues().add(value);
		value.setSection(Section.this);
	}

	public SectionType getSectionType() {
		return sectionType;
	}

	public void setSectionType(SectionType sectionType) {
		this.sectionType = sectionType;
	}
}